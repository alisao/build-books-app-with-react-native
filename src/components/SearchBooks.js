import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableHighlight,
  ActivityIndicator
} from 'react-native';
import SearchResults from './searchResults'

export default class SearchBooks extends Component {
  constructor(props) {
        super(props);
        this.state = {
            bookAuthor: '',
            bookTitle: '',
            isLoading: false,
            errorMessage: ''
        };
    }

    render() {
          var spinner = this.state.isLoading ?
              ( <ActivityIndicator
                  hidden='true'
                  size='large'/> ) :
              ( <View/>);
          return (
              <View style={styles.container}>
                  <Text style={styles.instructions}>Search by book title and/or author</Text>
                  <View style={styles.row}>
                      <Text style={styles.fieldLabel}>Book Title:</Text>
                      <TextInput style={styles.searchInput} onChange={this.bookTitleInput.bind(this)}/>
                  </View>
                  <View style={styles.row}>
                      <Text style={styles.fieldLabel}>Author:</Text>
                      <TextInput style={styles.searchInput} onChange={this.bookAuthorInput.bind(this)}/>
                  </View>
                  <TouchableHighlight style={styles.button}
                                      underlayColor='#0381a0'
                                      onPress={this.searchBooks.bind(this)}>
                      <Text style={styles.buttonText}>Search</Text>
                  </TouchableHighlight>
                  {spinner}
                  <Text style={styles.errorMessage}>{this.state.errorMessage}</Text>
              </View>
          );
      }

      bookTitleInput(event) {
          this.setState({ bookTitle: event.nativeEvent.text });
      }

      bookAuthorInput(event) {
          this.setState({ bookAuthor: event.nativeEvent.text });
      }

      searchBooks() {
          this.fetchData();
      }

      fetchData() {

          this.setState({ isLoading: true });

          var baseURL = 'https://www.googleapis.com/books/v1/volumes?q=';
          if (this.state.bookAuthor !== '') {
              baseURL += encodeURIComponent('inauthor:' + this.state.bookAuthor);
          }
          if (this.state.bookTitle !== '') {
              baseURL += (this.state.bookAuthor === '') ? encodeURIComponent('intitle:' + this.state.bookTitle) : encodeURIComponent('+intitle:' + this.state.bookTitle);
          }

          console.log('URL: >>> ' + baseURL);
          fetch(baseURL)
              .then((response) => response.json())
              .then((responseData) => {
                  this.setState({ isLoading: false});
                  if (responseData.items) {

                      this.props.navigator.push({
                          title: 'Search Results',
                          component: SearchResults,
                          passProps: {books: responseData.items}
                      });
                  } else {
                      this.setState({ errorMessage: 'No results found'});
                  }
              })
              .catch(error =>
                  this.setState({
                      isLoading: false,
                      errorMessage: error
                  }))
              .done();
      }
}

const styles = StyleSheet.create({
  container: {
        marginTop: 65,
        padding: 10,
    },
    row :{
      height:80
    },
    searchInput: {
      paddingLeft:5,
      fontSize: 18,
      borderWidth: 1,
      borderColor:'#7d7d7d',
      flex: 1,
    },
    button: {
        height: 50,
        backgroundColor: '#0099be',
        justifyContent: 'center',
        marginTop: 15
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },
    instructions: {
        fontSize: 18,
        alignSelf: 'center',
        marginBottom: 15
    },
    fieldLabel: {
        fontSize: 15,
        marginTop: 15
    },
    errorMessage: {
        fontSize: 15,
        alignSelf: 'center',
        marginTop: 15,
        color: 'red'
    }
});
