import React, {Component} from 'react';
import {View, Text, StyleSheet,TabBarIOS} from 'react-native';

import Featured from './featured';
import Search from './search';

export default class Root extends Component {
    constructor() {
        super();
        this.state = {
            selectedTab: 'featured'
        };
    }

    render() {
        return (
          //ViewPagerAndroid
            <TabBarIOS>
                <TabBarIOS.Item
                    selected={this.state.selectedTab === 'featured'}
                    systemIcon="featured"
                    onPress={() => {this.setState({selectedTab: 'featured'})}
                  }>
                    <Featured/>
                </TabBarIOS.Item>

                <TabBarIOS.Item
                    selected={this.state.selectedTab === 'search'}
                   systemIcon="search"
                    onPress={() => {
                    this.setState({selectedTab: 'search'});
                }}>
                    <Search/>
                </TabBarIOS.Item>
            </TabBarIOS>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});
