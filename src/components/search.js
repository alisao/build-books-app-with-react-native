import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  NavigatorIOS
} from 'react-native';
import SearchBooks from './SearchBooks'

export default class Search extends Component {
    render() {
        return (
          <NavigatorIOS
              barTintColor='#0099be'
              titleTextColor='#fff'
              tintColor='#fff'
              style={{flex:1}}
              initialRoute={{
              title: 'Search',
              component: SearchBooks
          }}/>
        );
    }
}


var styles = StyleSheet.create({
    description: {
        fontSize: 20,
        backgroundColor: 'white'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
