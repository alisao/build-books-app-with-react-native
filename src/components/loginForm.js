import React,{ Component } from 'react';
import {
  Text,
  View,
  AppRegistry,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  StatusBar
} from 'react-native';

export default class LoginForm extends Component {
  render (){
    return (
        <View>
          <StatusBar  barStyle="light-content" />

          <TextInput
          style={styles.input}
           placeholder="Enter Your email or username"
           placeholdeTextColor="#000"
           returnKeyType="next"
           keyboardType="email-address"
           autoCapitalize="none"
           autoCorrect={false}
           onSubmitEditing={()=>this.passwordInput.focus()}
          />

          <TextInput
          style={styles.input}
          secureTextEntry
          placeholder="Password"
          placeholdeTextColor="#000"
          returnKeyType="go"
          ref={(pass)=>this.passwordInput=pass}

          />

          <TouchableOpacity style={styles.submitBtn}>
            <Text style={styles.submitLabel}>LOGIN</Text>
          </TouchableOpacity>
        </View>
    )
  }
}

const styles=StyleSheet.create({
  input : {
    color:'#000',
    height:40,
    backgroundColor:"#fff",
    paddingHorizontal:20,
    marginBottom:10

  },
  submitBtn : {
    borderColor:'#fff',
    backgroundColor:'#0099be',
    padding:10,
    borderColor:'#fff',
    borderWidth:1

  },
  submitLabel : {
    color:'#fff',
    textAlign:'center',
    fontSize:18,
    lineHeight:30,
  }

})
