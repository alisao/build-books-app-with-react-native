import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  ListView,
  TouchableHighlight,
  ActivityIndicator
} from 'react-native';

import BookDetails from './bookDetails';
var FAKE_BOOK_DATA = [
    {volumeInfo: {title: 'The Catcher in the Rye', authors: "J. D. Salinger", imageLinks: {thumbnail: 'http://books.google.com/books/content?id=PCDengEACAAJ&printsec=frontcover&img=1&zoom=1&source=gbs_api'}}}
];


var REQUEST_URL = 'https://www.googleapis.com/books/v1/volumes?q=intitle:React%20Native';



export default class BookList extends Component {
  constructor() {
       super();
       this.state = {
           isLoading: "true",
           dataSource: new ListView.DataSource({
               rowHasChanged: (row1, row2) => row1 !== row2
           })
       };
   }

   componentWillMount() {
    this.fetchData();
   }

   fetchData() {
       fetch(REQUEST_URL)
       .then((response) => response.json())
       .then((responseData) => {
           this.setState({
              isLoading: "false",
               dataSource: this.state.dataSource.cloneWithRows(responseData.items)
           });
           
       })
       .done();
   }

   showBookDetail(book) {
          this.props.navigator.push({
              title: book.volumeInfo.title,
              component: BookDetails,
              passProps: {book}
          });
      }

   renderLoadingView() {
     return (
         <View style={styles.loading}>
         <ActivityIndicator
             animating={this.state.animating}
             style={[styles.centering, {height: 80}]}
             size="large"
           />
             <Text>
                 Loading books...
             </Text>
         </View>
     );
   }

   renderBook(book) {
       return (
            <TouchableHighlight onPress={() => this.showBookDetail(book)}  underlayColor='#dddddd'>
                <View>
                    <View style={styles.container}>
                        <Image
                            source={{uri:typeof(book.volumeInfo.imageLinks)!='undefined'?book.volumeInfo.imageLinks.thumbnail:'http://www.appcoda.com/wp-content/uploads/2015/04/react-native-1024x631.png'}}
                            style={styles.thumbnail} />
                        <View style={styles.rightContainer}>
                            <Text style={styles.title}>{book.volumeInfo.title}</Text>
                            <Text style={styles.author}>{book.volumeInfo.authors}</Text>
                        </View>
                    </View>
                    <View style={styles.separator} />
                </View>
            </TouchableHighlight>
       );
   }

   render() {
        if (this.state.isLoading=="true") {
            return this.renderLoadingView()
        }

        return (


             <ListView
                 dataSource={this.state.dataSource}
                 renderRow={this.renderBook.bind(this)}
                 style={styles.listView}
                 />

         );
 }


}

const styles = StyleSheet.create({
  container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    thumbnail: {
        width: 53,
        height: 81,
        marginRight: 10,
        marginLeft:10
    },
    rightContainer: {
        flex: 1
    },
    title: {
        fontSize: 20,
        marginBottom: 8
    },
    author: {
        color: '#656565'
    },
    separator: {
       height: 1,
       backgroundColor: '#dddddd'
   },
    listView: {
           backgroundColor: '#F5FCFF',
           marginTop:60,
           paddingBottom:80
       },
       loading: {
           flex: 1,
           alignItems: 'center',
           justifyContent: 'center'
       }
});
