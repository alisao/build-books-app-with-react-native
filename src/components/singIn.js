import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  KeyboardAvoidingView,
  Image,
  TextInput,
  TouchableOpacity,
  StatusBar
} from 'react-native';

import LoginForm from './loginForm';

export default class SignIn extends Component {
  gotoFav (){
    this.props.navigator.push ({
      id:'root'
    })
  }
  render() {
    return (
      <KeyboardAvoidingView style={styles.container} behavior="padding">

        <View style={styles.logoContainer}>
          <Image source={require('../../assets/images/app-icon.png')} style={styles.icon}/>
        </View>

        <View>
          <Text style={styles.appName}>
            {"The book app".toUpperCase()}
          </Text>
        </View>

        <View style={styles.formContainer}>
        <View>
          <StatusBar  barStyle="light-content" />

          <TextInput
           style={styles.input}
           placeholder="Enter Your email or username"
           placeholdeTextColor="#000"
           returnKeyType="next"
           keyboardType="email-address"
           autoCapitalize="none"
           autoCorrect={false}
           onSubmitEditing={()=>this.passwordInput.focus()}
          />

          <TextInput
          style={styles.input}
          secureTextEntry
          placeholder="Password"
          placeholdeTextColor="#000"
          returnKeyType="go"
          ref={(pass)=>this.passwordInput=pass}

          />

          <TouchableOpacity style={styles.submitBtn} onPress={this.gotoFav.bind(this)}>
            <Text style={styles.submitLabel}>LOGIN</Text>
          </TouchableOpacity>
        </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container : {
    flex:1,
    backgroundColor:'#0099be',
    justifyContent:'center',
    alignItems:'center'
  },
  icon:{
    width:300,
    height:300
  },
  appName :{
    color:'#fff',
    fontSize:18,
    marginTop:-70,
    backgroundColor:'transparent',

  },
  formContainer :{
    alignSelf:'stretch',
    paddingHorizontal :20,
    marginTop:120
  },
  input : {
    color:'#000',
    height:40,
    backgroundColor:"#fff",
    paddingHorizontal:20,
    marginBottom:10

  },
  submitBtn : {
    borderColor:'#fff',
    backgroundColor:'#0099be',
    padding:10,
    borderColor:'#fff',
    borderWidth:1

  },
  submitLabel : {
    color:'#fff',
    textAlign:'center',
    fontSize:18,
    lineHeight:30,
  }
});
