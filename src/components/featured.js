import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  NavigatorIOS
} from 'react-native';

import BookList from './BookList'

export default class Featured extends Component {
    render() {
        return (
          <NavigatorIOS
               style={{flex:1}}
               barTintColor='#0099be'
               titleTextColor='#fff'
               tintColor='#fff'
               initialRoute={{
                 title: 'Featured',
                 component: BookList
               }}/>
        );
    }
}


var styles = StyleSheet.create({
    description: {
        fontSize: 20,
        backgroundColor: 'white'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
