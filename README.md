# How to run this app

- For MAC users , download homebrew PM

[![N|Solid](https://brew.sh/img/homebrew-256x256.png)](https://brew.sh/)

- Open your terminal and install node
```sh
$ brew install node
```
- Or use download and install node executable

[![N|Solid](https://www.shareicon.net/data/128x128/2015/10/06/112725_development_512x512.png)](https://nodejs.org)

-Install React Native CLI with Node 
```sh
$ npm install -g react-native-cli
```

- CD to  [Build-Books-App-with-react-native](git@bitbucket.org:alisao/build-books-app-with-react-native.git) after cloning the repo to your local machine
```sh
$ cd path/To/Direcotry/Build-Books-App-with-react-native
```

-Run React Native project
```sh
$ react-native run-ios
-or-
$ react-native run-Android
```

-If the React Native bundler stopped for any reason . open new terminal tap and run the bundler alone 

```sh
$ react-native start
```