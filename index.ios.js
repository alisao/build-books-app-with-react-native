import React,{ Component } from 'react';
import { Navigator ,AppRegistry } from 'react-native';

import Root from './src/components/root';
import SignIn from './src/components/singIn';



export default class BooksApp extends Component {
  render() {
      return (
        <Navigator
          initialRoute={{ id: 'signIn'}}
          renderScene={this.renderNavScene.bind(this)}
        />
      );
    }

    renderNavScene (route,navigator){
      switch (route.id) {
        case 'signIn':
          return (<SignIn navigator={navigator}/>)
          break;
        case 'root':
          return (<Root navigator={navigator}/>)
          break;
        default:
        return (<Signin navigator={navigator}/>)
      }
    }
}



AppRegistry.registerComponent('BooksApp',()=>BooksApp);
